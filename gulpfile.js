var theme = 'sam';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var postcss = require('gulp-postcss');
var flexibility = require('postcss-flexibility');
var browserSync = require('browser-sync').create();
var minimalcss = './wp-content/themes/minimal-bootstrap/assets/css/*.scss';
var minimaljs = './wp-content/themes/minimal-bootstrap/assets/js/*.js';
var input_scss = [minimalcss, './wp-content/themes/' + theme + '/assets/css/*.scss'];
var input_js = [
    minimaljs,
    './wp-content/themes/' + theme + '/assets/js/scripts/*.js'
];
var output_css = './wp-content/themes/' + theme + '/assets/css/';
var output_js = './wp-content/themes/' + theme + '/assets/js/';
var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded'
};

gulp.task('scripts', function () {
    return gulp.src(input_js)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(output_js))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(output_js));
});

gulp.task('watch', function () {
    gulp.watch(input_scss, ['default']);
});

gulp.task('default', function () {
    return gulp
        .src(input_scss, {base: './'})
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(postcss([flexibility]))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('.'))
});

gulp.task('browserSync', function() {
  browserSync.init({
    proxy: proxyaddress
  });
});
