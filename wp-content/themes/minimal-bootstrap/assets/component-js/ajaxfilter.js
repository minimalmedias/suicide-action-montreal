/*
Ajax Loader
// pour faire fonctionner avec WPML il faut ajouter    'suppress_filters' => false au wp_query
// et passer la langue au call ajax. Voir "handing ajax calls with wpml"
*/

// Filter links/buttons
$(filterClass).on('click',function(e){
    e.preventDefault();
    $(filterClass).removeClass('active');
    $(this).addClass('active');
    var filtered = $(this).attr('href').substr(1);
    ajaxLoad(filtered, 0);
});

// Load more button
$('body').on('click', loadAreaId+' .pager', function (event) {
    event.preventDefault();
    var filtered = $(this).attr('data-filters');
    var page = $(this).attr('href').substr(7); //substring to strip #paged_ and get just number
    ajaxLoad(filtered, page);
});

function ajaxLoad(filtered,selectedpage) {
    $(loadAreaId).html(loaderTag);
    var firstload = false;
    if (selectedpage == 0) {
        firstload = true;
        selectedpage = 1;
    }
    $.ajax({
        method: "POST",
        data: {
            page: selectedpage,
            firstload: firstload,
            items_per_load: items_per_load,
            template: template,
            showposts: showposts,
            filter_by: filter_by,
            filtered: filtered,
        },
        url: "/wp-content/themes/minimal-bootstrap/ajaxfilter/ajaxloader.php",
        dataType: "html"
    }).success(function (response) {
        $(loadAreaId).html(response);
        if (!firstload) {
            if ($('#paged_' + selectedpage).length) {
                $('html,body').animate({scrollTop: $('#paged_' + selectedpage).offset().top - 200}, 800);
            }
        }
    }).error(function () {

    });
}