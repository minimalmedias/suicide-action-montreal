(function ($) {

    'use strict';

    $(document).ready(function () {

        responsivecarousel();

        $('.loadout').on('click',function(e){
            e.preventDefault();
            $('body').addClass('fadeaway');
            window.location = $(this).attr('href');
        });

        // Comments

        $('.commentlist li').addClass('card');
        $('.comment-reply-link').addClass('btn btn-secondary');

        // Forms

        $('select, input[type=text], input[type=email], input[type=password], textarea').addClass('form-control');
        $('input[type=submit]').addClass('btn btn-primary');

        // Pagination fix for ellipsis

        $('.pagination .dots').addClass('page-link').parent().addClass('disabled');

        // You can put your own code in here

        $('body').on('click', '.clickbox', function (event) {
            window.location.href = $(this).find('.clickurl').attr('href');
        });

    });

    window.addEventListener("resize", resizeThrottler, false);
    var resizeTimeout;

    function resizeThrottler() {
        if (!resizeTimeout) {
            resizeTimeout = setTimeout(function () {
                resizeTimeout = null;
                responsivecarousel();
            }, 66);
        }
    }

    function responsivecarousel() {
        var ww = $(window).width();
        if (ww < 768) {
            $('.responsive-carousel').carousel({interval: false});
        } else {
            $('.responsive-carousel').carousel({interval: 10000});
        }
    }

}(jQuery));

if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement('style')
    msViewportStyle.appendChild(
        document.createTextNode(
            '@-ms-viewport{width:auto!important}'
        )
    )
    document.head.appendChild(msViewportStyle)
}