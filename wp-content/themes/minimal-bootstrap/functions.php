<?php
/*
All the functions are in the PHP pages in the `functions/` folder.
*/


require get_template_directory() . '/functions/cleanup.php';
//require get_template_directory() . '/functions/menus.php';
require get_template_directory() . '/functions/setup.php';
require get_template_directory() . '/functions/enqueues.php';
require get_template_directory() . '/functions/navbar.php';
require get_template_directory() . '/functions/widgets.php';
require get_template_directory() . '/functions/customsearch.php';
require get_template_directory() . '/functions/search-widget.php';
require get_template_directory() . '/functions/index-pagination.php';
require get_template_directory() . '/functions/split-post-pagination.php';
require get_template_directory() . '/functions/feedback.php';
require get_template_directory() . '/functions/remove-query-string.php';
require get_template_directory() . '/functions/smk_themeview.php';
require get_template_directory() . '/functions/responsiveimage.php';
require get_template_directory() . '/functions/iefix.php';


