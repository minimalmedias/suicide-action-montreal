<?php
/*
AJAX FILTER ENGINE
Duplicate into child theme functions folder to customize
*/
require_once("../../../../wp-load.php");

$items_per_load = $_POST['items_per_load'];
$template = $_POST['template'];
$showposts = $_POST['showposts'];
$wp_query_array = array(
    'post_status' => 'publish',
    'showposts' => $showposts
);
$filter_by = json_decode($_POST['filter_by'], true); //convert jason to php array
$filtered = json_decode($_POST['filtered'], true); //convert jason to php array
if(!empty($filter_by)){
    foreach($filter_by as $key=>$value){ //set query with defaults
        $wp_query_array[$key] = $value;
    }
    foreach($filtered as $key=>$value){ //modify with specified filters
        if($value !=''){
            if(is_array($value)){
                foreach($value as $subkey=>$subvalue){
                    $wp_query_array[$key][$subkey] = $subvalue;
                }
            } else {
                $wp_query_array[$key] = $value;
            }
        }
    }
}
$wp_query = new WP_Query($wp_query_array);
$query_result = array();
if ($wp_query->have_posts()) {
    $i = 0;
    while ($wp_query->have_posts()) : $wp_query->the_post();
        $query_result[] = get_the_ID(); // build array of IDs of post results
        $i++;
    endwhile;
}

$chunks = array_chunk($query_result, $items_per_load);
$totalpages = count($chunks);
$nextpage = false;
$firstload = $_POST['firstload'];
if ($_POST['page'] < $totalpages) {
    $nextpage = $_POST['page'] + 1;
}
$cumulatedchunks = array();
foreach ($chunks as $index => $chunk) {
    if ($index < $_POST['page']) {
        $cumulatedchunks[] = $chunk;
    }
}

$i = 0;
foreach ($cumulatedchunks as $index => $posts) {
    foreach ($posts as $post_id) {
        $config['post_id'] = $post_id;
        // include extra options as needed.
        if ($i == $_POST['page']) { // Fadein animation only for newest page loaded
            $config['delay'] = 'style="animation-delay: ' . ($i / 2) . 's;"';
            // include class fadeInSequence for animation to work.
        }
        if ($ii == 0) {
            $config['rowid'] = 'paged_' . ($index + 1);
        }
        minimal_get_template_part($template, $config);
        /*  Template must include row wrapper with <?php echo (isset($this->rowid))?'id="'.$this->rowid.'"':''; ?> */
    }
    $i++;
}

if (isset($nextpage)) {
    $ajaxbutton['nextpage'] = $nextpage;
}
if (isset($items_per_load)) {
    $ajaxbutton['items_per_load'] = $items_per_load;
}
if ($_POST['type'] != 'home') {
    $ajaxbutton['datatype'] = $_POST['type'];
    minimal_get_template_part('/templates/ajaxbutton.php', $ajaxbutton);
}