<?php

function b4st_enqueues() {

	/* Styles */

  	//wp_register_style('b4st', get_template_directory_uri() . '/assets/css/style.css', false, null);
	//wp_enqueue_style('b4st');

	/* Scripts */

	wp_enqueue_script( 'jquery' );

    wp_register_script('modernizr',  'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', false, '2.8.3', false);
    wp_enqueue_script('modernizr');

	wp_register_script('popper',  'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js', false, '1.11.0', true);
	wp_enqueue_script('popper');

  	wp_register_script('bootstrap-js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js', false, '4.0.0-beta', true);
	wp_enqueue_script('bootstrap-js');

	wp_register_script('b4st-js', get_template_directory_uri() . '/assets/js/b4st.js', false, null, true);
	wp_enqueue_script('b4st-js');

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'b4st_enqueues', 100);
