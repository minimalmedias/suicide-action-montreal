<?php
add_action('admin_head', 'hide_editor'); //OPTION hide main editor for pages using only ACF forms
add_action('add_meta_boxes', 'add_gallery_metabox'); //OPTION add gallery metabox
add_action('save_post', 'gallery_meta_save'); //OPTION
add_action('admin_enqueue_scripts', 'gallery_metabox_enqueue'); //OPTION
function gallery_meta_save($post_id)
{
    if (!isset($_POST['gallery_meta_nonce']) || !wp_verify_nonce($_POST['gallery_meta_nonce'], basename(__FILE__))) return;
    if (!current_user_can('edit_post', $post_id)) return;
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
    if (isset($_POST['vdw_gallery_id'])) {
        update_post_meta($post_id, 'vdw_gallery_id', $_POST['vdw_gallery_id']);
    } else {
        delete_post_meta($post_id, 'vdw_gallery_id');
    }
}
function add_gallery_metabox($post_type)
{
    $types = array('page');
    if (in_array($post_type, $types)) {
        add_meta_box(
            'gallery-metabox',
            'Header Slide Show (2000px X 1021px)',
            'gallery_meta_callback',
            $post_type,
            'side',
            'high'
        );
    }
}
function gallery_meta_callback($post)
{
    wp_nonce_field(basename(__FILE__), 'gallery_meta_nonce');
    $ids = get_post_meta($post->ID, 'vdw_gallery_id', true);
    if ( function_exists('icl_object_id') ) {
        if(empty($ids)){
            $current_language= apply_filters( 'wpml_current_language', NULL );
            $opposite['en']='fr';
            $opposite['fr']='en';
            $translated_page_id = apply_filters( 'wpml_object_id', $post->ID, 'post', FALSE, $opposite[$current_language] );
            $ids = get_post_meta($translated_page_id, 'vdw_gallery_id', true);
        }
    }
    ?>
    <table class="form-table">
        <tr>
            <td>
                <a class="gallery-add button" href="#" data-uploader-title="Add image(s) to gallery"
                   data-uploader-button-text="Add image(s)">Add image(s)</a>

                <ul id="gallery-metabox-list">
                    <?php if ($ids) : foreach ($ids as $key => $value) : $image = wp_get_attachment_image_src($value); ?>

                        <li>
                            <input type="hidden" name="vdw_gallery_id[<?php echo $key; ?>]"
                                   value="<?php echo $value; ?>">
                            <img class="image-preview" src="<?php echo $image[0]; ?>">
                            <a class="change-image button button-small" href="#" data-uploader-title="Change image"
                               data-uploader-button-text="Change image">Change image</a><br>
                            <small><a class="remove-image" href="#">Remove image</a></small>
                        </li>

                    <?php endforeach; endif; ?>
                </ul>

            </td>
        </tr>
    </table>
<?php }
function gallery_metabox_enqueue($hook)
{
    if ('post.php' == $hook || 'post-new.php' == $hook) {
        wp_enqueue_script('gallery-metabox', get_stylesheet_directory_uri() . '/assets/component-js/gallery-metabox.js', array('jquery', 'jquery-ui-sortable'));
    }
}