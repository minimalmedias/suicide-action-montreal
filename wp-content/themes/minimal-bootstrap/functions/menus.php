<?php
function register_the_menus()
{
    register_nav_menus(array(
        'navbar' => 'navbar',
        'subnavbar' => 'subnavbar',
        'footerservicesnav' => 'footerservicesnav',
        'footersnav' => 'footersnav'
    ));
}