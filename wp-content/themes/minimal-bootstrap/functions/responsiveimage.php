<?php
# Display picture/srcset image easily
function responsiveimage($rimage){
    if(isset($rimage) and $rimage !='') {
        $responsiveimage = '<picture>';
        if (is_array($rimage['url'])) {
            $i = 0;
            foreach ($rimage['url'] as $key => $image) {
                if($image==''){
                    return;
                }
                $responsiveimage .= '<source srcset="' . $image . '" media="' . $key . '">';
                $i++;
            }
            $url = array_values(array_slice($rimage['url'], -1))[0];
        } else {
            $url = $rimage['url'];
        }
        $responsiveimage .= '<img src="' . $url . '" class="' . $rimage['classes'] . '" alt="' . $rimage['alt'] . '">';
        $responsiveimage .= '</picture>';
    } else {
        $responsiveimage = '';
    }
    return $responsiveimage;
}