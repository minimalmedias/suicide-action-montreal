<!DOCTYPE html>
<html class="no-js">
<head>
    <title><?php wp_title('•', true, 'right'); bloginfo('name'); ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="main-header">
    <?php
    minimal_get_template_part('/components/headernavigation.php', array(
        'homeurl' => esc_url(home_url('/')),
        'logo' => ' <img src="' . get_stylesheet_directory_uri() . '/assets/img/logo.png" class="img-fluid" alt="'.bloginfo('name').'">',
        'classes' => 'main-menu',
        'headerextra' => '<a href="/fr">fr</a>/<a href="/en">en</a>',
        'menuobject' => wp_nav_menu(array(
            'theme_location' => 'navbar',
            'container' => false,
            'menu_class' => '',
            'fallback_cb' => '__return_false',
            'items_wrap' => '<ul id="%1$s" class="navbar-nav mr-auto mt-2 mt-lg-0 %2$s">%3$s</ul>',
            'depth' => 2,
            'walker' => new b4st_walker_nav_menu()
        ))
    ));


    ?>
</header>
