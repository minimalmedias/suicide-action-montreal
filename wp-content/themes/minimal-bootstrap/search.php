<?php get_header(); ?>
<section id="search">
    <div class="container">
        <div class="row align-items-center">
            <div class="col text-center">
                <header>
                    <h1><?php _e('Search Results for', 'b4st'); ?> &ldquo;<?php the_search_query(); ?>&rdquo;</h1>
                </header>
                <hr/>
                <?php get_template_part('loops/content', 'search'); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
