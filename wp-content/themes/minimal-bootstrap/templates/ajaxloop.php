<div class="card fadeInSequence <?php echo $this->format; ?>" <?php echo (isset($this->delay)) ? $this->delay : ''; ?> <?php echo (isset($this->rowid)) ? ' id="' . $this->rowid . '"' : ''; ?>>
    <?php echo get_the_title($this->post_id); ?>
    <a href="<?php echo get_permalink($this->post_id); ?>" class="btn btn-primary"></a>
</div>