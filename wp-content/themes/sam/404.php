<?php get_header(); ?>
<?php minimal_get_template_part(
    '/templates/entete_simple.php',
    array('title' =>
        pll__('404')
    )
); ?>
<div class="container">
  <div class="row justify-content-center">
    
    <div class="col-md-5 text-center">
      <div id="content" role="main">
        <br>
        <div class="alert alert-warning">
          <p><?php _e('The page you were looking for does not exist.', 'b4st'); ?></p>
        </div>
      </div><!-- /#content -->
    </div>

  </div><!-- /.row -->
</div><!-- /.container -->

<?php get_footer(); ?>
