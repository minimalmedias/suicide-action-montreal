(function ($) {
    "use strict"; // Start of use strict

    var ww = $(window).width();


    if (ww > 991) {
        $('li.dropdown').addClass('hashover');
        $('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    } else {
        $('li.dropdown').removeClass('hashover');
        $('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }


    /*
    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 57)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });
    */

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function () {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 57
    });

    // Collapse Navbar
    var navbarCollapse = function () {
        if ($("#samheader").offset().top > 100) {
            $("#samheader").addClass("bg-gray2");
        } else {
            $("#samheader").removeClass("navbar-shrink");
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);


    var rtime;
    var ww = $(window).width();
    var timeout = false;
    var delta = 200;

    mobilemenu(ww);

    $(window).resize(function () {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeend, delta);
        }
    });

    function resizeend() {
        if (new Date() - rtime < delta) {
            setTimeout(resizeend, delta);
        } else {
            timeout = false;
            ww = $(window).width();
            mobilemenu(ww);
        }
    }

    function mobilemenu(ww) {
        if (ww > 991) {

        } else {

        }
    }

})(jQuery); // End of use strict




