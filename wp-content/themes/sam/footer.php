<footer id="footer" class="bg-darkgray">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col navcol">
                <a href="<?php echo home_url(); ?>" class="navbar-brand"><span class="icon-logo"></span></a>
            </div>
            <?php
            $auxmenu['fr'] = 'auxmenu';
            $auxmenu['en'] = 'auxmenu-en';
            foreach (wp_get_nav_menu_items($auxmenu[pll_current_language('slug')]) as $m): ?>
                <div class="col-auto auxmenu2">
                    <a href="<?php echo $m->url; ?>" title="<?php echo $m->attr_title; ?>"><?php echo $m->title; ?></a>
                </div>
            <?php endforeach; ?>
            <!--div class="col social text-right">
                <a href="#" class="footersocial icon-twitter"></a>
                <a href="#" class="footersocial icon-facebook"></a>
                <a href="#" class="footersocial icon-youtube"></a>
            </div-->

        </div>
    </div>
    <div class="light-gray-rule"></div>
    <div class="container">
        <div class="row">
            <div class="col footernumber">
                <p><?php echo pll__('Appelez-nous sans hésiter'); ?></p>
                <a href="tel:18662273553" class="no-wrap">1 866.277.3553</a>
            </div>
        </div>
    </div>
    <div class="light-gray-rule"></div>
    <div class="bg-darkgray">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col text-small lastline">
                    &copy;  <?php echo date('Y'); ?> Suicide Action Montréal,  Design/Dev:<a href="http://www.minimalmtl.com" class="minimal" target="_blank">Minimal</a>
                </div>
                <div class="col text-right langswitch">
                    <?php
                    $i=0;
                    foreach(pll_the_languages(array('raw'=>1)) as $l):
                        echo($i!=0)?' / ':'';
                        echo '<a class="langs" href="'.$l['url'].'">'.$l['slug'].'</a>';
                        $i++;
                    endforeach;
                    ?>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
