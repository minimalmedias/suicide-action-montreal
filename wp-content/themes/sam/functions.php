<?php
$parent =  $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/minimal-bootstrap';
require 'functions/functions-setup.php';
require 'functions/functions-setup-menus-pages.php'; // automaticaly create starter menu and pages. Do not use if installing theme on existing site.
require 'functions/functions-taxonomies.php';
require 'functions/functions-acf.php';
require 'functions/functions-navbar.php';
require 'functions/functions-breadcrumbs.php';
require 'functions/functions-general.php';
//require $parent.'/functions/gallery-metabox.php'; //Option for adding gallery in admin