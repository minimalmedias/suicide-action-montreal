<?php

add_theme_support('title-tag');
add_theme_support('post-thumbnails');
add_filter( 'body_class', function( $classes ) {return array_merge( $classes, array(
    'starter' //add classes to body here
) );} );

function extractterm($id, $type)
{
    $term = get_the_terms($id, $type);
    return $term[0]->name;
}

function get_submenu($id,$item){
    $return = [];
    $itemindex = [];
    $items = wp_get_nav_menu_items($id);
    $iterate=0;
    foreach($items as $i){
        if($i->menu_item_parent != 0) {
            $return[$i->menu_item_parent][$i->ID] = $i;
        } else {
            $itemindex[$iterate] = $i->ID;
            $iterate++;
        }
    }
    return $return[$itemindex[$item]];
}

function wp_get_attachment_image_url_with_fallback($id, $size)
{
    $imagesizes = get_image_sizes();
    $outputimage = wp_get_attachment_image_url($id, $size);
    if ($outputimage == '') {
        $outputimage ='http://via.placeholder.com/' . $imagesizes[$size]['width'] . 'x' . $imagesizes[$size]['height'];
    }
    return $outputimage;
}

function minimal_language_switcher()
{
    $languages = apply_filters('wpml_active_languages', NULL);
    if (!empty($languages)) {
        $return = '<ul>';
        foreach ($languages as $l) {
            //if (!$l['active'])
            $return .= '<li><a href="' . $l['url'] . '">' . $l['language_code'] . '</a></li>';
            //}
        }
        return $return . '</ul>';
    }
}

