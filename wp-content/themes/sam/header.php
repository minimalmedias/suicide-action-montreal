<!DOCTYPE html>
<html class="no-js" lang="<?php echo pll_current_language('slug'); ?>">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header  id="samheader">
    <div id="topbar">
        <div class="container">
            <div class="row">
                <div class="col-auto cell1">
                    <form id="searchform" class="form-inline" role="search" method="get"
                          action="<?php echo home_url('/'); ?>">
                        <input type="text" id="s" name="s" class="form-control pull-right"
                               placeholder="<?php echo pll__('Recherche'); ?>">
                        <input type="submit" value="" id="searchbtn">
                        <span class="btn icon-loupe" ></span>
                    </form>
                </div>
                <!--div class="col-auto cell2 mr-auto">
                    <a href="#" class="btn icon-twitter"></a>
                    <a href="#" class="btn icon-facebook"></a>
                    <a href="#" class="btn icon-youtube"></a>
                </div-->
                <div class="col-auto cell3 ml-auto">
                    <a href="tel:18662773553">1 866 277-3553</a>
                </div>
                <div class="col-auto cell4">
                    <?php
                    $i=0;
                    foreach(pll_the_languages(array('raw'=>1)) as $l):
                        echo($i!=0)?' / ':'';
                        echo '<a class="langs" href="'.$l['url'].'">'.$l['slug'].'</a>';
                        $i++;
                    endforeach;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    minimal_get_template_part('/components/headernavigation.php', array(
        'homeurl' => esc_url(home_url('/')),
        'logo' => '<div class="icon-logo"></div>',
        'id' => 'mainNav',
        'classes' => 'navbar navbar-expand-lg navbar-light',
        'headerextra' => "",
        'menuobject' => wp_nav_menu(array(
            'theme_location' => 'mainmenu',
            'container' => false,
            'echo' => false,
            'menu_class' => '',
            'fallback_cb' => '__return_false',
            'items_wrap' => '<ul id="%1$s" class="navbar-nav ml-auto">%3$s</ul>',
            'depth' => 2,
            'walker' => new sam_walker_nav_menu(),
            'navitem_a_classes' => 'nav-link js-scroll-trigger'
        ))
    ));
    ?>
</header>
