<?php
get_header();
if (get_field('header') == 'Homepage') {
    minimal_get_template_part('/templates/masthead.php', array(
        'title' => '<h1>' . pll__('SUICIDE ACTION MONTRÉAL') . '</h1>
                <h2>' . pll__('Une ligne de vie') . '</h2>
                <h3>' . pll__('1.866.APPELLE (277-3553)') . '</h3>
                <p>' . pll__('En détresse, inquiète, endeuillé; nous sommes là, ici et maintenant, 24/7.') . '</p>'
    ));
}
if (get_field('header') == 'Header simple (bleu foncé)') {
    minimal_get_template_part('/templates/entete_simple.php', array('title' => get_field('gros_titre')));
}
if (get_field('header') == 'Header avec menu (bleu pâle)') {
    minimal_get_template_part('/templates/entete_menu.php', array('title' => get_field('gros_titre')));
}
if (is_home()) {
} else {
    echo '<div class="container bg-b">';
    custom_breadcrumbs();
    echo '</div>';
}

if (have_rows('stack')):
    $i = 0;
    while (have_rows('stack')) : the_row();

        $args = array();
        $root = './wp-content/themes/sam';
        $directory = '/stacks/';
        $template = stackname(get_sub_field('gabarit'));
        if (file_exists($root . $directory . $template)):
            $get_args = get_args($root . $directory . $template);
            //if not supposed to have variables, force one and not empty
            if (empty($get_args)) {
                $args = array('spacer' => 1);
            } else {
                $args = array();
            }
            foreach ($get_args as $arg) {
                if (!empty(get_sub_field($arg))) {
                    $args[$arg] = get_sub_field($arg);
                }
            }
            if (get_sub_field('visible')):
                if (!empty($args)
                    or $template == 'stack_c_-_contenu_fixe.php'
                    or $template == 'social.php'
                    or $template=='nouvelles.php'
                ): //if supposed to have variables and they are not empty
                    $args['id'] = 'stack' . $i;
                    minimal_get_template_part($directory . $template, $args);
                endif;
            endif;
            $i++;
        else:
            $args = array();
            echo '<div class="alert alert-danger m-5 text-center">';
            echo 'Missing template file "' . $template . '" in directory "' . $directory . '"';
            echo '</div>';
        endif;

    endwhile;
endif;

function stackname($stack)
{
    $stack = str_replace(' ', '_', strtolower($stack));
    return $stack . '.php';
}

function get_args($file)
{
    $file = file_get_contents($file);
    preg_match_all('/\$this->[A-Za-z0-9-_]+/', $file, $vars);
    $return = array();
    foreach ($vars[0] as $a) {
        $return[] = str_replace('$this->', '', $a);
    }
    return $return;
}

get_footer();
?>
