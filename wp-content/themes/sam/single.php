<?php get_header(); ?>
<?php
if (have_posts()): while (have_posts()):
    the_post();
    ?>

    <article class="articleheading">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="articlehead">
                        <?php
                        if (has_post_thumbnail()):
                            echo '<img src="'.get_the_post_thumbnail_url(get_the_ID(),'full').'" alt="'.get_the_title() .'" class="img-fluid">';
                        endif;
                        ?>
                        <h5><?php the_time('d-m-Y') ?></h5>
                        <h1 class="h2"><?php the_title() ?></h1>
                        <!--p><?php the_author(); ?></p-->
                        <p class="blurb"><?php //the_excerpt();
                            ?></p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-8 articlecontent">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </article>
    <!--section>
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    [instagram]
                </div>
            </div>
        </div>
    </section-->
<?php endwhile; ?>
<?php else: ?>
    <?php wp_redirect(get_bloginfo('url') . '/404', 404);
    exit; ?>
<?php endif; ?>
<nav class="bottomnav">
    <div class="container">
        <?php next_post_link('<span class="navback">%link</span>', pll__('Article précédant')); ?>
        <span class="icon"></span>
        <?php next_post_link('<span class="navforward">%link</span>', pll__('Article suivant')); ?>
    </div>
</nav>
<?php minimal_get_template_part('/templates/newsletter.php', array()); ?>
<?php get_footer(); ?>

