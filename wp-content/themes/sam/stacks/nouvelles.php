<?php
$query_result = array();
$args = array('post_status' => 'publish');

if (isset($this->homepage)){
    $args['posts_per_page'] = 2;
}

$wp_query = new WP_Query($args);
if ($wp_query->have_posts()):
    $i = 0;
    while ($wp_query->have_posts()) : $wp_query->the_post();
        $id = get_the_ID();
        $thumbnail = get_the_post_thumbnail_url($id, 'newsthumbnail');
        $query_result[$i]['photo'] = get_the_post_thumbnail_url($id);
        $query_result[$i]['date'] = get_the_time('d-m-Y');
        $query_result[$i]['title'] = get_the_title();
        $query_result[$i]['excerpt'] = get_the_excerpt($id);
        $query_result[$i]['permalink'] = get_permalink();
        $i++;
    endwhile;
endif;
wp_reset_query();
if (!empty($query_result)):
    ?>
    <section class="my-5 <?php echo (isset($this->homepage))?'py-5':''; ?> stack_nouvelles <?php echo ($this->id == 'stack0') ? '' : 'reveal'; ?>">
        <div class="container">
            <?php if (isset($this->homepage)): ?>
                <div class="row">
                    <div class="col text-center mb-5"><h2><?php echo pll__('Actualités'); ?></h2></div>
                </div>
            <?php endif; ?>
            <div class="row justify-content-center">
                <?php
                foreach ($query_result as $news):
                    minimal_get_template_part('/templates/news-nothumbnail.php', $news);
                endforeach;
                ?>
            </div>
            <?php if (isset($this->homepage)):
                $lang = pll_current_language('slug');
                $pagename['fr'] = 1820;
                $pagename['en'] = 1822;
                ?>
                <div class="row">
                    <div class="col text-center">
                        <a href="<?php echo get_permalink($pagename[$lang]); ?>" class="btn btn-primary">
                            <?php echo pll__('Voir plus'); ?>
                        </a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif;
?>