<section id="twitterbox" class="bg-paleblue pb-5 stack_nouvelles">
    <div class="container p-5">
        <!--div class="row">
            <div class="col text-center">
                <a class="twitterlink" href="#">
                    <span class="icon-twitter d-block"></span>
                    <span class="label">@suicideactionmontreal</span>
                </a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-9 px-5 text-center bg-white">
                <p class="mt-4">@SAM @appelezmoisam Lorem ipsum Vivamus adipiscing fermentum quam volutpat aliquam. Integer et elit
                    eget elit facilisis tristique. Nam vel iaculis mauris.</p>
                <p class="small mb-4">Publié 3:05 pm, 20 nov, 2017</p>
            </div>
        </div>
        <div class="row">
            <div class="col text-center p-4">
                <a href="#" class="link-text">SUIVEZ-NOUS</a>
            </div>
        </div-->
        <div class="row">
            <div class="col text-center mb-5">
                <?php if(isset($this->contenu)): ?>
                <?php
                    $shortcode = str_replace(array('<p>','</p>'),array('',''),$this->contenu);
                    echo do_shortcode( $shortcode );
                    //echo do_shortcode('[arrow_sf id="1846"]');
                ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>