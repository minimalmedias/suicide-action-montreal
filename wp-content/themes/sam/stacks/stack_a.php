<section class="stack_a <?php echo ($this->id=='stack0')?'':'reveal'; ?>">
    <div class="container bg-b">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <?php if(isset($this->titre)): echo $this->titre; endif; ?>
                <div class="bar <?php echo ($this->titre=='')?'notitle':''; ?>"></div>
                <?php if(isset($this->contenu)): echo $this->contenu; endif; ?>
                <?php if(isset($this->boutons)): foreach ($this->boutons as $bouton): ?>
                    <p><a href="<?php echo $bouton['url']; ?>" <?php echo($bouton['open_blank'])?'target="_blank"':''; ?> class="btn btn-primary"><?php echo $bouton['label']; ?></a></p>
                <?php endforeach; endif; ?>
            </div>
        </div>
    </div>
</section>