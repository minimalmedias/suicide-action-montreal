<section class="stack_b <?php echo ($this->id=='stack0')?'':'reveal'; ?>">
    <div class="container bg-b">
        <div class="row justify-content-center" id="<?php echo $this->id; ?>">
        <?php $i=0; foreach($this->icons as $icon): ?>
            <div class="col-md-3 text-center">
                <div role="button" <?php echo( $icon['contenu']!='')?'data-toggle="collapse"':''; ?> data-parent="#<?php echo $this->id; ?>" data-target="#icon<?php echo $this->id.'_'.$i; ?>" aria-expanded="false" aria-controls="icon<?php echo $this->id.'_'.$i; ?>">
                <div class="icon <?php echo $icon['icon']; ?>"></div>
                <h5><?php echo $icon['titre']; ?></h5>
                </div>
                <div class="collapse collapsecontent" id="icon<?php echo $this->id.'_'.$i; ?>">
                    <?php echo $icon['contenu']; ?>
                </div>
            </div>
        <?php $i++; endforeach; ?>
        </div>
    </div>
</section>