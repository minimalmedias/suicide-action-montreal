<section class="stack_c <?php echo ($this->id=='stack0')?'':'reveal'; ?>">
    <div class="container bg-white p-3 p-xl-5 text-center">
        <h3 class="big"><?php echo pll__('Appelez-nous sans hésiter'); ?></h3>
        <h2 class="big"><?php echo pll__('1.866.Appelle'); ?></h2>
    </div>
    <div class="container bg-white subnumber">
        <h2 class="text-ligthtgray blueborderleft"><?php echo pll__('1.866.(277-3553)'); ?></h2>
    </div>
</section>