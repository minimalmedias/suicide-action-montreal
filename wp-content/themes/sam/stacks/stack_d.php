<section class="stack_d <?php echo ($this->id=='stack0')?'':'reveal'; ?>">
    <div class="container bg-primary">
        <div class="row justify-content-center">
            <div class="col-lg-6 p-lg-5 text-white">
                <?php if(isset($this->titre)): echo $this->titre; endif; ?>
            </div>
            <div class="col-lg-6 p-lg-5 text-white">
                <?php if(isset($this->contenu)): echo $this->contenu; endif; ?>
            </div>
        </div>
    </div>
</section>