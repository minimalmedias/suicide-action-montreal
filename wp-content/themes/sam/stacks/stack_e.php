<section class="stack_e <?php echo ($this->id=='stack0')?'':'reveal'; ?>">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 px-5 titre">
                <?php if(isset($this->titre)): echo $this->titre; endif; ?>
            </div>
            <div class="col-lg-7 px-5 barred">
                <?php if(isset($this->contenu)): echo $this->contenu; endif; ?>
            </div>
        </div>
    </div>
</section>