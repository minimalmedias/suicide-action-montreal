<section class="stack_f <?php echo ($this->id=='stack0')?'':'reveal'; ?>">
    <div class="container bg-b py-4 p-md-5">
        <div class="row px-4">
            <div class="w-100 pb-3">
                <?php if(isset($this->sous_titre)){ echo $this->sous_titre; } ?>
                <?php if(isset($this->titre)){ echo $this->titre; } ?>
            </div>
        </div>
        <div class="row px-4" id="<?php echo $this->id; ?>">
            <?php
            $i=0;
            foreach($this->collapse_box as $box):
            ?>
            <div class="card w-100">
                <div class="card-header" id="heading<?php echo $this->id.'_'.$i; ?>">
                    <h5 class="mb-0">
                        <button role="button" class="<?php echo ($i==0)?'':'collapsed'; ?>" data-toggle="collapse" data-target="#collapse<?php echo $this->id.'_'.$i; ?>" aria-expanded="true" aria-controls="collapse<?php echo $this->id.'_'.$i; ?>">
                            <?php echo $box['titre']; ?>
                        </button>
                    </h5>
                </div>
                <div id="collapse<?php echo $this->id.'_'.$i; ?>" class="collapse <?php echo ($i==0)?'show':''; ?>" aria-labelledby="heading<?php echo $this->id.'_'.$i; ?>" data-parent="#<?php echo $this->id; ?>">
                    <div class="card-body">
                        <?php echo $box['contenu']; ?>
                    </div>
                </div>
            </div>
            <?php $i++; endforeach; ?>
        </div>
    </div>
</section>