<section class="stack_g my-3 my-xl-5 <?php echo ($this->id=='stack0')?'':'reveal'; ?>">
    <div class="container">
        <div class="row align-items-end justify-content-end ">
            <div class="col-xl-6 text-xl-right order-1 order-xl-2">
                <div class="bg-pale p-4  py-md-5">
                    <div class="blueborderright"></div>
                    <div class="px-md-5">
                        <div class="mt-xl-5"> <?php if(isset($this->titre)): echo $this->titre; endif; ?></div>
                        <div class="emphasize"> <?php if(isset($this->sous_titre)): echo $this->sous_titre; endif; ?></div>
                        <?php if(isset($this->contenu)): echo $this->contenu; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>