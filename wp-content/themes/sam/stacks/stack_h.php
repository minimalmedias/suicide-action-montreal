<section class="stack_h my-xl-5 py-xl-5 <?php echo ($this->id=='stack0')?'':'reveal'; ?>">
    <div class="container">
        <div class="row justify-content-start">
            <div class="col-xl-7">
                <div class="text-left bg-pale p-4 p-md-5">
                    <div class="blueborderleftvertical"></div>
                    <div class="px-md-5 ml-xl-5">
                        <div class=""> <?php if(isset($this->titre)): echo $this->titre; endif; ?></div>
                        <div class="emphasize"> <?php if(isset($this->sous_titre)): echo $this->sous_titre; endif; ?></div>
                        <?php if(isset($this->contenu)): echo $this->contenu; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>