

<section class="my-xl-5 py-xl-5">
    <div class="container">
        <div class="row bg-pale">
            <div class="col-xl-4 text-xl-left">
                <div class="mx-4 mx-md-5">
                    <h2 class="mt-4 mt-md-5">Apprendre à prévenir le suicide</h2>
                </div>
                <div class="blueborderleft"></div>
            </div>
            <div class="col-xl-8 text-md-left">
                <div class="mx-4 mx-md-5 m-xl-5">
                    <p class="mb-xl-5">Nos services s'étendent également à la formation de sentinelles ou à leur soutien
                        ainsi qu'à de
                        nombreuses ressources spécifiquement développées pour les intervenants ou les organisations
                        touchées par le suicide. On peut apprendre à prévenir le suicide soit en se formant, en accédant
                        à de l'information ou en trouvant les bonnes ressources. Voici des pistes de bases
                        efficaces.</p>
                    <div class="container-fluid mt-xl-5">
                        <div class="row bg-pale py-4 py-md-5 mb-5 mb-xl-0">
                            <div class="col-lg-4 col-xl-6">
                                <p class="mb-4 mr-md-4 mr-xl-0 iconed icon-document singleline">PAR LA FORMATION</p>
                            </div>
                            <div class="col-lg-4 col-xl-6">
                                <p class="mb-4 iconed icon-bibliotheque singleline">PAR LA DOCUMENTATION</p>
                            </div>
                            <div class="col-lg-4 col-xl-6 mt-xl-5">
                                <p class="mb-4 iconed icon-moniteur multiline"><span class="d-md-block">PAR DES</span>
                                    RESSOURCES EN LIGNE</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="my-lg-5 py-5">
    <div class="container">
        <div class="row align-items-end justify-content-end ">
            <div class="col-xl-6 text-xl-right order-1 order-xl-2">
                <div class="bg-pale py-5">
                    <div class="blueborderright"></div>
                    <div class="px-md-5 mx-5">
                        <h2 class="mt-xl-5">Intervenir pour prévenir le suicide</h2>
                        <p>Vous pouvez aller plus loin dans la prévention du suicide en vous impliquant d'une manière
                            financière ou en donnant de votre temps. Voici quelques possibilités pour soutenir cette
                            lutte
                            essentielle. </p>
                        <div class="container-fluid my-5 pb-4">
                            <div class="row">
                                <div class="col-md-4">
                                    <p class="minheight iconed icon-lettre text-center">EN DONNANT</p>
                                </div>
                                <div class="col-md-4">
                                    <p class="minheight iconed icon-mains text-center">EN DEVENANT BÉNÉVOLES</p>
                                </div>
                                <div class="col-md-4">
                                    <p class="minheight iconed icon-groupe text-center">EN IMPLIQUANT SON ORGANISATION</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="my-xl-5 py-xl-5">
    <div class="container">
        <div class="row">
            <div class="col text-center"><h2>Actualités</h2></div>
        </div>
        <div class="row">
            <?php
            $news = array(
                'date' => '11 NOVEMBRE 2017',
                'title' => 'Remise du Prix Lise-Morin',
                'excerpt' => 'Le Prix Lise-Morin est le prix le plus prestigieux qu’un bénévole puisse recevoir à SAM depuis 1989. Il est offert à …',
                'permalink' => '#',
                'photo' => get_stylesheet_directory_uri() . '/assets/img/phototest.jpg'
            );
            foreach (array($news, $news) as $news):
                ?>
                <div class="col-xl-6">
                    <div class="my-5 boxoverlay">
                        <img src="<?php echo $news['photo']; ?>" class="image" alt="<?php echo $news['title']; ?>">
                        <div class="box">
                            <h5><?php echo $news['date']; ?></h5>
                            <h3><?php echo $news['title']; ?></h3>
                            <p><?php echo $news['excerpt']; ?></p>
                            <p class="text-right">
                                <a href="#" class="btn btn-link">Lire plus</a>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="row">
            <div class="col text-center pb-5 pb-xl-0"><a href="#" class="mt-xl-5 btn btn-primary">Voir plus</a></div>
        </div>
    </div>
</section>

<?php minimal_get_template_part('/templates/twitter-stack.php', array(
    'stackclasses' => 'bg-paleblue pb-5','id'=>'twitterbox'
));?>