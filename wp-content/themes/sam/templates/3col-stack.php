<section <?php echo (isset($this->stackclasses)?'class="'.$this->stackclasses.'"':'').' '.(isset($this->id)?'id="'.$this->id.'"':''); ?>>
    <div class="container bg-pale">
        <div class="row justify-content-center mt-4 mt-xl-0">
            <div class="col-lg-4 col-xl-3 text-center text-lg-left">
                <ul class="list-unstyled mx-auto mx-lg-5 m-xl-5 pt-md-2 text-charcoal">
                    <li>« Je vais me foutre en l’air »</li>
                    <li>« Je ne vous embêterai plus »</li>
                    <li>« Vous allez avoir la paix »</li>
                    <li>« Je n’en peux plus »</li>
                    <li>« Je voudrais disparaître »</li>
                </ul>
            </div>
            <div class="col-lg-4">
                <div class="mx-5 pt-5 pt-lg-0 m-md-0 m-xl-5 text-center text-lg-left">
                    <p class="iconed icon-chat multiline  mb-5 mb-md-3"><span class="d-lg-block">LES MESSAGES VERBAUX</span>
                        DIRECTS ET
                        INDIRECTS</p>
                    <p class="iconed icon-facepense multiline mb-4 mb-md-3"><span class="d-lg-block">LES INDICES</span>
                        COGNITIFS</p>
                    <p class="iconed icon-nuages multiline mb-5 mb-md-3"><span class="d-lg-block">LES SYMPTÔMES DE</span>
                        DÉPRESSION</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="mx-5 pb-5 pb-xl-0 m-md-0 m-xl-5">
                    <p class="iconed icon-face multiline mb-5 mb-md-3"><span class="d-lg-block">LES INDICES</span>
                        COMPORTEMENTAUX</p>
                    <p class="iconed icon-coeur multiline mb-5 mb-md-3"><span class="d-lg-block">LES INDICES</span> ÉMOTIONNELS
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>