<section <?php echo (isset($this->stackclasses)?'class="'.$this->stackclasses.'"':'').' '.(isset($this->id)?'id="'.$this->id.'"':''); ?>>
    <div class="container bg-primary text-white">
        <div class="row align-items-center">
            <div class="col-xl-6">
                <div class="p-4 p-md-5">
                    <h2 class="text-white m-0">Pour en savoir plus sur nos services spécifiques, vous pouvez consulter
                        les
                        liens suivants :</h2>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="px-4 px-md-5 pt-0 p-xl-5 mb-5 mb-xl-0">
                    <ul class="biglist">
                        <li>Services aux personnes en détresse ou suicidaires</li>
                        <li>Services aux proches qui s'inquiètes</li>
                        <li>Services aux personnes endeuillées</li>
                        <li>Services aux organisations touchées par le suicide (postvention)</li>
                        <li>Services aux intervenants et aux organismes</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>