<section <?php echo (isset($this->stackclasses)?'class="'.$this->stackclasses.'"':'').' '.(isset($this->id)?'id="'.$this->id.'"':''); ?>>
    <div class="container bg-white p-3 p-xl-5 text-center">
        <h3 class="big">Appelez-nous sans hésiter</h3>
        <h2 class="big">1.866.Appelle</h2>
    </div>
    <div class="container bg-white subnumber">
        <h2 class="text-ligthtgray blueborderleft">1.866.(277-3553)</h2>
    </div>
</section>