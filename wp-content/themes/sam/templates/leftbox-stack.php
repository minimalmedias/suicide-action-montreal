<section <?php echo (isset($this->stackclasses)?'class="'.$this->stackclasses.'"':'').' '.(isset($this->id)?'id="'.$this->id.'"':''); ?>>
    <div class="container">
        <div class="row justify-content-start">
            <div class="col-xl-7">
                <div class="text-left bg-pale p-4 p-md-5">
                    <div class="blueborderleftvertical"></div>
                    <div class="px-md-5 ml-xl-5">
                        <h2 class="blueborderright">L'important est d'en parler et de ne pas rester dans la
                            solitude.</h2>
                        <p class="emphasize">Nous sommes un service confidentiel et disponible <span class="no-wrap">24 heures sur 24</span>, <span class="no-wrap">7 jours sur
                                7.</span></p>
                        <p class="h2">1 866.227.3553</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>