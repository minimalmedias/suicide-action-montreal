<div class="masthead container text-center text-white d-flex">
    <div class="container my-3 my-lg-5 my-xl-auto titlezone">
        <div class="row align-items-center">
            <div class="col text-center mx-auto">
                <?php echo $this->title; ?>
            </div>
        </div>
    </div>
    <div class="container subnavzone">
    <?php
    echo wp_nav_menu(array(
        'theme_location' => 'submenu',
        'container' => false,
        'echo' => false,
        'menu_class' => 'submenu',
        'fallback_cb' => '__return_false',
        'items_wrap' => '<ul id="%1$s" class="nav">%3$s</ul>',
        'depth' => 2,
        'walker' => new minimal_walker_nav_menu(),
        'navitem_a_classes' => 'nav-link'
    ))
    ?>
    </div>
</div>