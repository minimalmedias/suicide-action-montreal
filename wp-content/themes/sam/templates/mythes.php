<div class="row">
    <div class="col-md bg-lightgreen question">
        <div class="grid-square-cell">
            <div class="align-items-center">
                <div class="text-center">
                    <h2 class="text-blue"><?php echo $this->title; ?></h2>
                    <h3><?php echo $this->subtitle; ?></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md bg-lightergreen reponse">
        <div class="grid-square-cell">
            <div class="align-items-center">
                <div class="text-center">
                    <h2 class="text-blue"><?php echo $this->answertitle; ?></h2>
                    <p><?php echo $this->answertext; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
