<div class="col-xl-6 card-deck">
    <div class="card box">
        <div class="card-body">
            <h5><?php echo $this->date; ?></h5>
            <h3><?php echo $this->title; ?></h3>
            <p><?php echo $this->excerpt; ?></p>
        </div>
        <div class="card-footer">
            <p class="text-right">
                <a href="<?php echo $this->permalink; ?>" class="btn btn-link">
                    <?php echo pll__('Lire plus'); ?>
                </a>
            </p>
        </div>
    </div>
</div>