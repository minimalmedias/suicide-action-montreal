<div class="col-xl-6">
    <div class="my-5 boxoverlay clearfix">
        <img src="<?php echo $this->photo; ?>" class="image" alt="<?php echo $this->title; ?>">
        <div class="box">
            <h5><?php echo $this->date; ?></h5>
            <h3><?php echo $this->title; ?></h3>
            <p><?php echo $this->excerpt; ?></p>
            <p class="text-right">
                <a href="<?php echo $this->permalink; ?>" class="btn btn-link">Lire plus</a>
            </p>
        </div>
    </div>
</div>