<section class="'my-3 my-xl-5">
    <div class="container">
        <div class="row align-items-end justify-content-end ">
            <div class="col-xl-6 text-center text-md-right pb-5 order-2 order-xl-1">
                <a href="#" class="btn btn-primary mt-5 mt-xl-0 mx-5">En savoir plus</a>
            </div>
            <div class="col-xl-6 text-xl-right order-1 order-xl-2">
                <div class="bg-pale p-4  py-md-5">
                    <div class="blueborderright"></div>
                    <div class="px-md-5">
                        <h2 class="mt-xl-5">Suicide Action Montréal est là, ici et maintenant</h2>
                        <p>Suicide Action Montréal propose aux Montréalais un éventail de services qui s'adressent
                            autant
                            aux personnes en détresse ou suicidaires, à leurs proches qui s'inquiètent, ou à ceux et
                            celles
                            qui ont vécu le suicide d'un être cher et doivent composer avec un deuil imprévisible.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>