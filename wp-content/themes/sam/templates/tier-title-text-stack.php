<section <?php echo (isset($this->stackclasses)?'class="'.$this->stackclasses.'"':'').' '.(isset($this->id)?'id="'.$this->id.'"':''); ?>>
    <div class="container bg-pale">
        <div class="row align-items-center justify-content-center firstrow">
            <div class="col-xl-4 pt-5 pt-xl-0 text-center">
                <h2 class="section-heading px-3 px-lg-5 px-xl-0 m-xl-5 text-left"><?php echo get_field('home_bloc-1-1_blurb'); ?></h2>
            </div>
            <div class="col-xl-7 text-center">
                <p class="text-left big-text brightblue px-3 px-lg-5 p-xl-5 mx-xl-5 pl-xl-5 left-paleblue-border"><?php echo get_field('home_bloc-1-2_blurb'); ?></p>
            </div>
        </div>
    </div>
</section>